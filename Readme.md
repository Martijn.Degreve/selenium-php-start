# Test Automation PHP

This sample project helps you to get started with test automation in PHP

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Required software

```
PHP
Composer 
```

### Installing

Install dependencies

```
composer install
```




## Start Selenium server
Make sure the chrome driver for your correct chrome version is downloaded and added to the path
Start the server in the commandline using the following command
```
chromedriver --port=4444
```

## Running the tests
### Commandline

Execute in root of the project

### Mac ###

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/TestsShouldGoHere.php
```

### Windows ###

```
"vendor/bin/phpunit" --bootstrap ./vendor/autoload.php ./tests/TestsShouldGoHere.php
```

## Built With

* [Composer](https://getcomposer.org/) - Dependency Management
* [PHPUnit](https://phpunit.de/index.html) - Unit testing framework
