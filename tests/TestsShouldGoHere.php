<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class TestsShouldGoHere extends TestCase
{
 protected $webDriver;

  public function setUp(): void
  {
    $capabilities = DesiredCapabilities::chrome();
    $this->webDriver = RemoteWebDriver::create('http://localhost:4444', $capabilities);
  }

  public function tearDown(): void
  {
    $this->webDriver->quit();
  }

  public function test_searchTextOnGoogle()
  {

  }

}